1 libavstream

  iavstream致力于为广大安卓应用开发者提供高效，简洁的免费音视频流化解决方案。推出的libavstream流媒体引擎可以灵活实现音频、视频和音视频的流化传输。支持的传输模式包括RTMP、RTSP协议的c/s模式和MPEG-TS流格式的p2p模式，支持的编码格式为视频H.264,音频格式为AAC。适用于搭载安卓系统的智能手机、平板电脑、智能电视、智能眼镜以及车载设备等移动终端。值得一提的是，libavstream的使用方式非常简单，在Android Studio开发环境中Module对应的build.gradle文件中加入一行代码即可实现所有功能：

dependencies {

    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.android.support:appcompat-v7:23.0.1'
    compile 'com.iavstream:libavstream:1.1.0'
    
}

需要强调的是，同时需要在Project的的build.gradle文件中将仓库设置为jcenter

buildscript
{

    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:1.3.0'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}


allprojects {

    repositories {
        jcenter()
    }
}


2 BLife

    BLife 是使用libavstream1.1.0库开发的一款开源安卓手机音视频直播APP。该应用由五个fragment构成，分别为主页面、直播页面、短信分享页面、直播列表页面和播放收看页面。

    除此之外，在menu菜单还有一个设置页，可以进行直播参数的相关设置，视频相关为分辨率、帧率、输出码率以及前后摄像头的自由选择，音频相关类为输出码率。 输出方面支持MPEG-TS流的点到点模式传输和RTSP模式的广播式传输，如果选择MPEG-TS则需要选择传输目的节点的IP地址和端口，如果选择RTSP广播模式，则需要设置RTSP流媒体服务器的地址和直播会话的SDP文件名称。
    与BLife配套的RTSP服务器为Darwin Streaming Server，安装文件及安装方法请参考本项目附件进行下载和安装。
