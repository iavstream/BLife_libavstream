package com.iavstream.blife.ui;


import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.iavstream.blife.R;


public class MyplayerActivity extends Activity implements
                        OnBufferingUpdateListener, 
						OnCompletionListener, 
						MediaPlayer.OnPreparedListener, 
						SurfaceHolder.Callback
{
	private static final String	TAG	= "chase";
	private int					mVideoWidth;
	private int					mVideoHeight;
	private MediaPlayer			mMediaPlayer;
	private SurfaceView			mPreview;
	private SurfaceHolder		holder;
	private String				path;
	private boolean             playerPrepared=false;
	

	public void onCreate(Bundle icicle)
	{
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.fullplayer);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		mPreview = (SurfaceView) findViewById(R.id.surface_play);
		holder = mPreview.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

	}
	private void playVideo()
	{
		try
		{
			 Bundle bundle = this.getIntent().getExtras();
		     path = bundle.getString("path");
		     DisplayToast(path);

			 mMediaPlayer = new MediaPlayer();
			 mMediaPlayer.setDataSource(path);
			 mMediaPlayer.setDisplay(holder);
			 mMediaPlayer.prepare();

			mMediaPlayer.setOnBufferingUpdateListener(this);
			mMediaPlayer.setOnCompletionListener(this);
			mMediaPlayer.setOnPreparedListener(this);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			
		}
		catch (Exception e)
		{
			//Log.e(TAG, "error: " + e.getMessage(), e);
		}
	}
	public void onBufferingUpdate(MediaPlayer arg0, int percent)
	{
		//Log.d(TAG, "onBufferingUpdate percent:" + percent);
	}
	public void onCompletion(MediaPlayer arg0)
	{
		//Log.d(TAG, "onCompletion called");
	}
	public void onPrepared(MediaPlayer mediaplayer)
	{
		//Log.d(TAG, "onPrepared called");
	}

	public void surfaceChanged(SurfaceHolder holder,int format, int width, int height)
	{
		//Log.d(TAG, "surfaceChanged called:");
		mVideoWidth = width;
		mVideoHeight = height;
		if (mVideoWidth != 0 && mVideoHeight != 0)
		{
			holder.setFixedSize(mVideoWidth, mVideoHeight);
	    	mMediaPlayer.start();
	    }
	}
	public void surfaceDestroyed(SurfaceHolder surfaceholder)
	{
		//Log.d(TAG, "surfaceDestroyed called");
	}
	public void surfaceCreated(SurfaceHolder holder)
	{
		//Log.d(TAG, "surfaceCreated called");
		playVideo();
	}
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (mMediaPlayer != null)
		{   mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

	}
	
	public void DisplayToast(String str)
	{		
	    Toast.makeText(this, str, Toast.LENGTH_LONG).show();	
	}
}
