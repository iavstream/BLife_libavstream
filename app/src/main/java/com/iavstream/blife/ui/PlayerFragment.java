
package com.iavstream.blife.ui;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.iavstream.blife.BLifeApplication;
import com.iavstream.blife.R;
import com.iavstream.utility.StreamAddressStuff;


public class PlayerFragment extends Fragment implements 
                             OnBufferingUpdateListener, 
                             OnCompletionListener, 
                             MediaPlayer.OnPreparedListener, 
                             SurfaceHolder.Callback

{
	private static final String	TAG	= "chase";
	private int					mVideoWidth;
	private int					mVideoHeight;
	private MediaPlayer			mMediaPlayer=null;
	private SurfaceView			mPreview;
	private SurfaceHolder		mHolder;
	private String				mPath=null;
	private View mBuffering;         //ProgressBar for display buffering
	
	//private boolean             mPlayerPlaying=false;
	private Context mContext = null;
	private View mRootView;
	private Button mStartStop,mPause,mFullscreen;
	
	private BLifeApplication mApplication;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.myplayer,container,false);
		super.onCreate(savedInstanceState); 	   
		initView();
		setListener();
		return mRootView;
	}
	
	
	void initView(){
		    mContext = (BLifeActivity)getActivity();
		    mApplication = (BLifeApplication)getActivity().getApplication();
		    
			mPreview = (SurfaceView)mRootView.findViewById(R.id.surface_play);			
			mHolder = mPreview.getHolder();			
			mHolder.addCallback(this);		
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);			
					
			mStartStop=(Button)mRootView.findViewById(R.id.btn_startstop);
			mPause=(Button)mRootView.findViewById(R.id.btn_pause);
			mFullscreen=(Button)mRootView.findViewById(R.id.btn_fullscreen);
			mBuffering=(View)mRootView.findViewById(R.id.buffering);
			
			mBuffering.setVisibility(View.GONE);
	}
	
   void setListener() {
	   
	   mStartStop.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				
				if(mPath==null){
					DisplayToast("尚未选择任何文件");
					return;
				}
				if(mMediaPlayer==null){
					mBuffering.setVisibility(View.VISIBLE);					
					PlayerPrepare();
					mStartStop.setText("停止");
				}
				else{
					mMediaPlayer.stop();
					mMediaPlayer.release();
					mMediaPlayer = null;
					mStartStop.setText("开始");
					System.gc();
				}	
						
	     }			
	 });		
		
		
	  mPause.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if(mMediaPlayer==null){
					DisplayToast("请先开启播放");
					return;				
				}

				if(mMediaPlayer.isPlaying()){						
					mMediaPlayer.pause();
					mPause.setText("继续");
				}
				else
				{					
					try
					{					
						mMediaPlayer.reset();
						mMediaPlayer.setDataSource(mPath);						
						mMediaPlayer.prepare();			
			     		
					}
					catch (Exception e)
					{
						Log.e(TAG, "error: " + e.getMessage(), e);
					}				   
					mPause.setText("暂停");						
				}			
			}
		});
		
		mFullscreen.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				
				if(mPath==null){
					DisplayToast("尚未选择任何文件");
					return;
				}
				
                if(mMediaPlayer==null){
					
					
					
				}
				else{
					mMediaPlayer.stop();
					mMediaPlayer.release();
					mMediaPlayer = null;
					mStartStop.setText("开始");
					System.gc();
				}
				
				Intent intent = new Intent(mContext.getApplicationContext(),MyplayerActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("path",mPath);
				intent.putExtras(bundle);
				startActivity(intent);
			}		
		});   
   }  
	
	private void PlayerPrepare()
	{
		try
		{	
			mMediaPlayer = new MediaPlayer();			
			mMediaPlayer.setDataSource(mPath);			
			mMediaPlayer.setDisplay(mHolder);
			mMediaPlayer.prepareAsync();
     		mMediaPlayer.setOnBufferingUpdateListener(PlayerFragment.this);
			mMediaPlayer.setOnCompletionListener(PlayerFragment.this);
			mMediaPlayer.setOnPreparedListener(PlayerFragment.this);
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);			
		}
		catch (Exception e)
		{
			Log.e(TAG, "error: " + e.getMessage(), e);
		}
	}
	
	
	public void onBufferingUpdate(MediaPlayer arg0, int percent)
	{

	}
	public void onCompletion(MediaPlayer arg0)
	{

	}
	public void onPrepared(MediaPlayer mediaplayer)
	{
		mBuffering.setVisibility(View.GONE);
		mMediaPlayer.start();
	}
	
	public void surfaceCreated(SurfaceHolder holder)
	{

	}

	public void surfaceChanged(SurfaceHolder holder,int format, int width, int height)
	{ 


	}
	public void surfaceDestroyed(SurfaceHolder surfaceholder)
	{

		if (mMediaPlayer != null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

		
	}
	
    @Override  
    public void setUserVisibleHint(boolean isVisibleToUser) {  
        super.setUserVisibleHint(isVisibleToUser);  
        if (isVisibleToUser) {
        	
        	StreamAddressStuff ss=new StreamAddressStuff();
        	 if(mMediaPlayer==null&&mApplication.jumpEnabled){
        		//DisplayToast(mApplication.FromName+mApplication.FromAddress);
        		mApplication.jumpEnabled=false;
        		mPath=ss.loaduserpwd(mApplication.FromAddress);
     			mBuffering.setVisibility(View.VISIBLE);					
     			PlayerPrepare();
     			mStartStop.setText("停止");
     			
     		}        	
        	
        } else {

        	 if(mMediaPlayer==null){

     			
     		}
     		else{
     			mMediaPlayer.stop();
     			mMediaPlayer.release();
     			mMediaPlayer = null;
     			mStartStop.setText("开始");
     			System.gc();
     			
     		}	
        	
        }
 }
	public void DisplayToast(String str)
	{		
	    Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();	
	}
	

}






