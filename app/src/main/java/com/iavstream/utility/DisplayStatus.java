package com.iavstream.utility;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.iavstream.libavstream.MediaParameter;


 public class DisplayStatus extends View{

     private  MediaParameter mp = null;

    public DisplayStatus(Context context){
        super(context);
        mp = null;
    }
    public  DisplayStatus(Context context, AttributeSet attrs) {
		super(context, attrs);
		mp = null;
		Log.i("chase", "DisplayStatus:DisplayStatus");
	}
    public boolean SetMediaParameter(MediaParameter parameter)
    {
    	   if(parameter==null)
               return false;
    		mp=parameter;
			return true;
    }
    @Override 
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mp==null)return;
        
        Paint loadPaint = new Paint();
        loadPaint.setAntiAlias(true);
        loadPaint.setTextSize(16);
        loadPaint.setARGB(255, 255, 255, 255);
        canvas.drawText("parameter set:", 4, 152, loadPaint);

        String loadText;
        loadText = "video cam side:"+(mp.video_camside==1?"front":"back")+", res:"+mp.video_width+"*"+mp.video_height+" fps: "+mp.video_fps+", bitrate:"+mp.video_bitrate/1000+"kbps";
        canvas.drawText(loadText, 4, 172, loadPaint);
        loadText = "audio bitrate:" + mp.audio_bitrate/1000+" kbps ";
        canvas.drawText(loadText, 4, 192, loadPaint);
        loadText = "output format type:"+(mp.formattype==1?"video and audio":(mp.formattype==2?"only video ":"only audio"));
        canvas.drawText(loadText, 4, 212, loadPaint);
        loadText = "output stream type:"+(mp.streamtype==1?"mpeg-ts":"rtsp")+",stream address:"+(mp.streamtype==1?mp.tsaddress+":"+mp.portname:mp.ipaddress+"/"+mp.sdpfilename);
        canvas.drawText(loadText, 4, 232, loadPaint);

        updateDisplay();
    }

    void updateDisplay() {
        invalidate();
    }
}
