package com.iavstream.utility;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.TextUtils;

import java.util.ArrayList;


public class GetContactInfo {	

	 Context mContext = null;
	 private static final int PHONES_DISPLAY_NAME_INDEX = 0;
	 private static final int PHONES_NUMBER_INDEX = 1;
     private static final String[] PHONES_PROJECTION = new String[] {Phone.DISPLAY_NAME, Phone.NUMBER};
	 private ArrayList<String> mContactsName = new ArrayList<String>();
	 private ArrayList<String> mContactsNumber = new ArrayList<String>();
	 public GetContactInfo(Context c)
      {    	  
    	  mContext=c;    	  
      }
	 public ArrayList<String> GetContactName()
      {
    	  return mContactsName;
      }
	 public ArrayList<String> GetContactNumber()
      {
    	  return mContactsNumber;
      }

    // @SuppressLint("NewApi")
	public void getPhoneContacts()
	{
	   ContentResolver resolver = mContext.getContentResolver();
	   Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,PHONES_PROJECTION, null, null, null);
	   if (phoneCursor != null) {
	    while (phoneCursor.moveToNext())
	    {
		  String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
		  if (TextUtils.isEmpty(phoneNumber))continue;
		  String contactName = phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX);
		  mContactsName.add(contactName);
		  mContactsNumber.add(phoneNumber);
	    }
	    phoneCursor.close();
	   }
    }

    private void getSIMContacts()
	{
	  ContentResolver resolver = mContext.getContentResolver();
	  Uri uri = Uri.parse("content://icc/adn");
	  Cursor phoneCursor = resolver.query(uri, PHONES_PROJECTION, null, null,	null);

	  if (phoneCursor != null) {
	      while (phoneCursor.moveToNext())
		 {
		    String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);
		    if (TextUtils.isEmpty(phoneNumber))continue;
		    String contactName = phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX);
		    mContactsName.add(contactName);
		    mContactsNumber.add(phoneNumber);
	     }
	    phoneCursor.close();
	  }

	}
    
}
