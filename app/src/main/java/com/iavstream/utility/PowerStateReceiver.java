
package com.iavstream.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.widget.Toast;

public class PowerStateReceiver extends BroadcastReceiver{
	
	   static final public String TAG = "PoliceSpy_PowerStateReceiver";
	   Context mContext;
	   boolean wifiopen=false;
   	   boolean mobileopen=false;

   	@Override
	public void onReceive(Context context, Intent intent) {
   		mContext=context;
		int batteryLevel = intent.getIntExtra("level", 0);
		if(batteryLevel<10)DisplayToast("the battery level is lower than 10%");
	}
        
    public void DisplayToast(String str)
    {
	    	Toast toast = Toast.makeText(mContext, str,Toast.LENGTH_SHORT);
	    	toast.setGravity(Gravity.BOTTOM, 0, 220);
	    	toast.show();
	}
      
   }  