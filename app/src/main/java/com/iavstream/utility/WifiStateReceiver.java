
package com.iavstream.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.os.Parcelable;
import android.view.Gravity;
import android.widget.Toast;

public class WifiStateReceiver extends BroadcastReceiver{

	   static final public String TAG = "PoliceSpy_WifiStateReceiver";
	   Context mContext;
	   boolean wifiopen=false;
   	   boolean mobileopen=false;
	
        @Override  
        public void onReceive(Context context, Intent intent) {

            mContext=context;

        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
        	  
                int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);                
                switch (wifiState) {   
                case WifiManager.WIFI_STATE_DISABLED:
                	DisplayToast("wifiState: disabled");                	
                    break;   
                case WifiManager.WIFI_STATE_DISABLING:
                	DisplayToast("wifiState: disabling");
                    break;
                case WifiManager.WIFI_STATE_ENABLED:
                	DisplayToast("wifiState: enabled");
                    break;
               case WifiManager.WIFI_STATE_UNKNOWN:
            	   DisplayToast("wifiState: unknown");
                    break;
                }   
            }

        if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {
        	
                Parcelable parcelableExtra = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO); 
                
                if (null != parcelableExtra) {    
                    NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;    
                    State state = networkInfo.getState();  
                    boolean isConnected = state==State.CONNECTED;
                    if(isConnected)
                    {  
                    	DisplayToast("wifiState: open");   
                    }
                    else
                    {  
                    	DisplayToast("wifiState: close");  
                    }  
                }    
            }  

        if(ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())){  
            NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);  
            if (info != null) {
            	if(info.getTypeName().endsWith("WIFI")&&info.getState().toString().endsWith("CONNECTED"))
            	{
            		DisplayToast("wifi open");
            	}
            	else if(info.getTypeName().endsWith("mobile")&&info.getState().toString().endsWith("CONNECTED"))
            	{
            		DisplayToast("3g  open");
            	}
            	
            }   
          }
        }
        public void DisplayToast(String str)
	    {
	    	Toast toast = Toast.makeText(mContext, str,Toast.LENGTH_SHORT);
	    	toast.setGravity(Gravity.BOTTOM, 0, 220);
	    	toast.show();
	    }
        
      
    }  